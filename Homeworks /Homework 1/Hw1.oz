%================= QUE 1.1 ==================
declare
fun {Take Xs N}
   if N == 0 then nil else  
      case Xs
      of nil then nil
      [] X|Xr then X|{Take Xr N-1}
      end
   end
end

{Browse {Take [1 2 3 ] 0}}

%================== QUE 1.2 ==================
declare
fun {Drop Xs N}
   if N == 0 then Xs else
      case Xs
      of nil then nil
      [] X|Xr then {Drop Xr N-1}
      end
   end
end

{Browse {Drop [1 2 3] 1}}

%================== QUE 1.3 ===================
declare
fun {CrossProduct Xs Ys}
   
   local Link in
      fun {Link Z Ls Partial}
	 case Ls
	 of nil then Partial
	 [] L|Lr then [Z L]|{Link Z Lr Partial}
	 end
      end
      
      case Xs#Ys
      of nil#nil then nil
      [] (X|Xr)#nil then nil
      [] nil#(Y|Yr) then nil
      [] (X|Xr)#(Y|Yr) then {Link X Ys {CrossProduct Xr Ys}}
   end
   end
end

{Browse {CrossProduct [1 2 3] [a b c]}}

%================= QUE 2.1 =======================
declare
fun {Filter F Xs}
   case Xs
   of nil then nil
   [] X|Xr then if {F X} then X|{Filter F Xr} else {Filter F Xr} end
   end
end

{Browse {Filter fun {$ X} X > 0 end [1 2 3 4 5 ~1 2 3 ~1 4]}}

%================ QUE 2.2 ========================
% This is not tail recursive
declare
fun {FoldR F Xs Partial}
   case Xs
   of nil then Partial
   [] X|Xr then {F X {FoldR F Xr Partial}}
   end
end

declare
fun {Sum A B}
A + B
end

{Browse {FoldR Sum [1 2 3 4] 0}}
%=============== QUE 2.3 ==========================
% Map using foldR
% F_unary is a Unary function to be operated on all the elements of the list

declare
fun {FoldR F Xs Partial}
   case Xs
   of nil then Partial
   [] X|Xr then {F X {FoldR F Xr Partial}}
   end
end

declare
fun {Map F_Unary Xs}
      {FoldR fun {$ X List} {F_Unary X}|List end Xs nil}
end

{Browse {Map fun {$ X} X*X end [1 4 9]}}
%================ QUE 3: Using map and fold =======
%function to check if S occours in the list Ts
%Using lexical scoping
declare
fun {Check_In_List S Ts} 
   case Ts
   of nil then false
   [] T|Tr then S==T orelse {Check_In_List S Tr}
   end
end

%Standard Map Function 
declare
fun {Map F Xs}
   case Xs
   of nil then nil
   [] X|Xr then {F X}|{Map F Xr}
   end
end

%Standard FoldL Function
declare
fun {FoldL F Partial Xs}
   case Xs
   of nil then Partial
   [] X|Xr then {FoldL F  {F Partial X} Xr}
   end
end

%Xs is a list of literal and Ys is a list of list of literls
declare
fun {Count Xs Ys}
   case Xs
   of nil then nil
   [] X|Xr then [X {FoldL fun {$ X Y} X + Y end 0 {Map fun {$ Ls} if {Check_In_List X Ls} then 1 else 0 end end Ys }}]|{Count Xr Ys}
   end
end

{Browse {Count [dabangg planetOfTheApes] [[planetOfTheApes is boring] [kick is terrible] [dabangg rocks] [kick was good]]}}


%================ QUE 3 ===========================

declare
fun {Count Xs Ys}
   local Check_In_List Count_Occour in
%Helper Function
      fun {Check_In_List S Ts} %function to check if S occours in the list
	 case Ts
	 of nil then false
	 [] T|Tr then S==T orelse {Check_In_List S Tr}
	 end
      end

      fun {Count_Occour F S Ts} % Ts is a list of list, F is a function == Filtering Function
	 case Ts
	 of nil then 0
	 [] T|Tr then if { F S T} then 1 + {Count_Occour F S Tr } else {Count_Occour F S Tr}
		      end
	 end
      end
    
      case Xs
      of nil then nil
      [] X|Xr then [X {Count_Occour Check_In_List X Ys}]| {Count Xr Ys}
      end
   end
end

{Browse {Count [planetOfTheApes kick] [[planetOfTheApes is boring] [kick is terrible] [dabangg rocks]]}}

%=============== QUE 3: Using even better Higher-Order Programming =============

%Helper Functions
declare
fun {Check_In_List S Ts} %function to check if S occours in the given list
   case Ts
   of nil then false
   [] T|Tr then S==T orelse {Check_In_List S Tr}
   end
end

declare
fun {Count_Occour F S Ts} % Ts is a list of list, F is a function == Filtering Function
   case Ts
   of nil then 0
   [] T|Tr then if {F S T} then 1 + {Count_Occour F S Tr } else {Count_Occour F S Tr}
		end
   end
end

%It uses lexical scoping to get the definitions of Count_Occour and Check_In_List.
declare
fun {Count Xs Ys}
      case Xs
      of nil then nil
      [] X|Xr then [X {Count_Occour Check_In_List X Ys}]| {Count Xr Ys}
      end
end

%If you don't want to use lexical scoping, then you may use
%fun {Count Func1 Func2 Xs Ys}
%      case Xs
%      of nil then nil
%      [] X|Xr then [X {Func1 Func2 X Ys}]| {Count Xr Ys}
%      end
%end
%{Browse {Count Count_Occour Check_In_List  [planetOfTheApes kick] [[planetOfTheApes is boring] [kick is terrible] [dabangg rocks]] }}
    

{Browse {Count [planetOfTheApes kick] [[planetOfTheApes is boring] [kick is terrible] [dabangg rocks] [kick was good]]}}

%================ QUE 4 : Tail Recursive fibonacci ===========================
% I am considering the fibonacchi series as 0,1,1,2,... with the start index as 1. So 0 corresponds to N = 1.
declare
fun {Fibo N Partial Partial_N}
   case N
   of 1 then Partial
   else {Fibo N-1 Partial_N Partial + Partial_N}
   end
end

{Browse {Fibo 5 0 1}}

%==================== QUE 5 : Lazy Seive ==========================
declare
fun {Filter F Xs}
   case Xs
   of nil then nil
   [] X|Xr then if {F X} then X|{Filter F Xr} else {Filter F Xr} end
   end
end


declare
fun lazy {Filter_Lazy F Xs}
   case Xs
   of nil then nil
   [] X|Xr then if {F X} then X|{Filter F Xr} else {Filter F Xr} end
   end
end

declare
fun lazy {Make_Seive Xs}
   case Xs
   of X|Xr then X|{Make_Seive {Filter_Lazy fun {$ Y} Y mod X \= 0 end  Xr}}
   end
end

declare
fun lazy {Make_String N}
   N|{Make_String N+1}
end

declare H =  {Make_Seive {Make_String 2}}

{Browse {Nth H 18}}
%======================= QUE 6 : Lazy Exponentiation ====================
%Assume that X is a float here. i.e. give 1.0 instead of 1
declare
fun lazy {ExpSeries X}
   local Gen_Series in
      fun lazy {Gen_Series X N Start}
	 ((Start*X)/N) | {Gen_Series X N+1.0 (Start*X)/N}
      end
      1.0|{Gen_Series X 1.0 1.0}
   end
end

declare E = {ExpSeries 1.0}
%Thus the above function lazily generates the Infinte series for e^X
%The approximant function takes an infinte list as an input, therefore, We do not need to specify the base case
%Epsilon is a float here
%I am ignoring if two consecutive terms are same
declare
fun {Approximant Epsilon Xs}
   if {Nth Xs 1} == {Nth Xs 2} then
%this has been introduced as the first two terms are same in the series of E^X for base case of 1
      Xs.1 + {Approximant Epsilon Xs.2}
   else
      if ({Nth Xs 1} - {Nth Xs 2}) =< Epsilon andthen (({Nth Xs 1} - {Nth Xs 2}) >= ~1.0*Epsilon) then
	 Xs.1
      else
	 Xs.1 + {Approximant Epsilon Xs.2}
      end
   end
end

{Browse {Approximant 0.0001 E}}
%======================= QUE 7 ====================

declare
fun {Rand_Average Count}
   local Generate_Infinite Average X Sum Y in
      fun lazy {Generate_Infinite}
	     ({OS.rand} mod 2)|{Generate_Infinite}
      end

      fun {Average Xs Count Partial_Sum}
	    if(Count == 0)
	    then Partial_Sum
	    else
	       case Xs of X|Xr then {Average Xr Count-1 Partial_Sum + X} end
	    end
      end
      if Count == 0
      then
	 0
      else
	 thread X = {Generate_Infinite} end
	 thread Sum = {Average X Count 0} end
	 {IntToFloat Sum}/{IntToFloat Count}
      end   
   end
end

{Browse {Rand_Average 10}}

%=================The End===========================

