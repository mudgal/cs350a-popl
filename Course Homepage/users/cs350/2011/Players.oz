declare NewPort2 Player

% A Stateless port object
fun {NewPortObject2 Proc}
   Sin in
   thread
      for Msg in Sin do
	 {Proc Msg}
      end
   end
   {NewPort Sin}
end

% Modelling a player.
% Others is a tuple consisting of the ports representing the other
% players.
% Id is the player's identification
fun {Player Others Id}
   {NewPortObject2
    proc {$ Msg}
       case Msg of ball then
	  Next = {OS.rand} mod {Width Others} + 1
       in
	  {Browse Id}
	  {Delay 1500}
	  {Send Others.Next ball}
       end
    end}
end



% Creating a game
% P1, P2, P3 are port objects representing players.
declare P1 P2 P3
P1 = {Player others(P2 P3) 1}
P2 = {Player others(P1 P3) 2}
P3 = {Player others(P1 P2) 3}
{Send P1 ball}


