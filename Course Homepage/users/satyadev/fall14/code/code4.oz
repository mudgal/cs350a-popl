%==============
% Lazy list merge
%==============
declare
fun lazy {Merge Xs Ys}
   case Xs#Ys
   of (X|Xr)#(Y|Yr) then
      if X < Y then X|{Merge Xr Ys}
      elseif X>Y then Y|{Merge Xs Yr}
      else X|{Merge Xr Yr}
      end
   end
end

%================
% Lazy Times
%================
fun lazy {Times N Xs}
   case Xs
   of nil then nil
   [] X|Xr then N*X|{Times N Xr}
   end
end

%=======================
% Builds the Hamming List
% Also uses partially evaluated functions
%=======================
declare H

H = 1 | {Merge {Times 2 H} {Merge {Times 3 H} {Times 5 H}}}


{Browse H}
{Browse {List.take H 6}}

